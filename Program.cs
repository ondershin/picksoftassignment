﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Picksoft_Assignment
{

    public class Grid
    {
        #region VARIABLES
        private bool isInitialized;
        #endregion

        #region PROPERTIES
        public bool IsInitialized => isInitialized;
        #endregion

        public void Initialize(Command command)
        {
            Debug.Assert(!isInitialized);

            string[] inputs = command.commandString.Split(' ');
            int rowCount, columnCount;

            bool success = int.TryParse(inputs[0], out rowCount);
            success = success && int.TryParse(inputs[1], out columnCount);

            Debug.Assert(success);

            isInitialized = true;
        }

    }

    public interface IRover
    {
        Rover.Orientations Orientation { get; }
        void Move(Command command);
    }

    public class Rover : IRover
    {
        public enum Orientations
        {
            NORTH = 0,
            SOUTH = 1,
            EAST = 2,
            WEST = 3,
        }

        #region VARIABLES
        private Vector2 position;
        private Orientations orientation;
        #endregion

        #region PROPERTIES
        public Orientations Orientation => orientation;
        #endregion

        public void Move(Command command)
        {

        }
    }

    public class GameManager
    {
        #region SUBSYSTEMS
        private readonly Grid grid;
        #endregion

        #region VARIABLES
        private readonly Queue<IRover> roverQueue;
        #endregion

        public GameManager()
        {
            grid = new Grid();
            roverQueue = new Queue<IRover>();
        }

        public void OnCommand(Command command)
        {
            Debug.Assert(command.commandType != CommandType.NONE);
            ProcessCommand(command);
        }

        private void ProcessCommand(Command command)
        {
            switch (command.commandType)
            {
                case CommandType.GRID_INITIALIZE:
                    if (!grid.IsInitialized)
                    {
                        grid.Initialize(command);
                    }
                    break;
                case CommandType.ROVER_INITIALIZE:
                    break;
                case CommandType.ROVER_MOVE:
                    break;

                default:
                    Debug.Assert(false, command.commandType.ToString());
                    break;
            }
        }
    }

    public enum CommandType
    {
        NONE = 0,
        GRID_INITIALIZE = 1,
        ROVER_INITIALIZE = 2,
        ROVER_MOVE = 3
    }

    public struct Command
    {
        public CommandType commandType;
        public string commandString;

        public Command(CommandType commandType, string commandString)
        {
            this.commandType = commandType;
            this.commandString = commandString;
        }
    }

    public static class CommandFactory
    {
        public static Command CreateCommand(CommandType commandType, string input)
        {
            return new Command(commandType, input);
        }
    }

    public class InputHandler
    {
        private struct VerificationResult
        {
            public enum Status
            {
                VALID = 0,
                INVALID_CHARS = 1,
                INVALID_COMMAND_PATTERN = 2,
                UNKNOWN_COMMAND_PATTERN = 3
            }

            public readonly Status status;
            public readonly CommandType commandType;

            public VerificationResult(Status status, CommandType commandType = CommandType.NONE)
            {
                this.status = status;
                this.commandType = commandType;
            }
        }

        public void DemandInput(Action<CommandType, string> inputCallback)
        {
            Debug.Assert(inputCallback != null, "Callback method cannot be null.");

            Console.WriteLine("Waiting for input_ : ");

            string input = Console.ReadLine().ToUpper();
            VerificationResult result = VerifyInput(input);

            if (result.status == VerificationResult.Status.VALID)
            {
                // invoke callback with valid command input.
                inputCallback.Invoke(result.commandType, input);
            }
            else
            {
                Console.WriteLine($"/tInput is not valid. Error : [{result.status.ToString()}]");
            }

        }

        /// <summary>
        ///   Check input string against predefined rule set inorder to verify
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private VerificationResult VerifyInput(string input)
        {
            char[] validChars = new char[] { 'L', 'R', 'M', 'N', 'S', 'W', 'E', ' ' }; // should be cached.
            if (input.Any(c => !validChars.Contains(c) || !char.IsDigit(c)))
            {
                return new VerificationResult(VerificationResult.Status.INVALID_CHARS);
            }

            // Three rule check remains for valid char sets.
            /*
                    ? : [0 - 9] digit
                    
                    Board creation command syntax         : "? ?" ,           starts with digit
                    Rover initialization command syntax   : "? ? [N,S,W,E]"   starts with digit
                    Move command syntax                   : "[L*R*M*]*"       only [L,R,M]
             */

            if (char.IsDigit(input[0]))
            {
                var inputs = input.Split(' ');
                if (inputs.Length == 2)
                {
                    if (!int.TryParse(inputs[0], out int t1) || !int.TryParse(inputs[1], out int t2))
                    {
                        return new VerificationResult(VerificationResult.Status.INVALID_COMMAND_PATTERN);
                    }
                    else
                    {
                        // valid
                        return new VerificationResult(VerificationResult.Status.VALID, CommandType.GRID_INITIALIZE);
                    }
                }
                else if (inputs.Length == 3)
                {
                    if (!int.TryParse(inputs[0], out int t1) || !int.TryParse(inputs[1], out int t2))
                    {
                        return new VerificationResult(VerificationResult.Status.INVALID_COMMAND_PATTERN);
                    }

                    char[] orientations = new char[] { 'N', 'S', 'W', 'E' }; // should be cached.
                    if (inputs[2].Length != 1 || !orientations.Contains(inputs[2][0]))
                    {
                        return new VerificationResult(VerificationResult.Status.INVALID_COMMAND_PATTERN);
                    }
                    else
                    {
                        // valid
                        return new VerificationResult(VerificationResult.Status.VALID, CommandType.ROVER_INITIALIZE);
                    }
                }
                else
                {
                    return new VerificationResult(VerificationResult.Status.UNKNOWN_COMMAND_PATTERN);
                }
            }
            else
            {
                char[] moves = new char[] { 'L', 'R', 'M' };
                if (input.Any(c => !moves.Contains(c)))
                {
                    return new VerificationResult(VerificationResult.Status.INVALID_COMMAND_PATTERN);
                }
                else
                {
                    // valid
                    return new VerificationResult(VerificationResult.Status.VALID, CommandType.ROVER_MOVE);
                }
            }
        }
    }

    public class Game
    {
        #region SUBSYSTEMS
        readonly InputHandler inputHandler;
        #endregion

        #region VARIABLES
        private bool isActive;
        #endregion

        #region PROPERTIES
        public bool IsActive => isActive;
        #endregion

        public Game()
        {
            inputHandler = new InputHandler();
        }

        public void Start()
        {
            if (isActive)
            {
                Debug.Assert(false, "Game is already running.");
                return;
            }

            OnStart();
        }

        private void OnStart()
        {
            isActive = true;
            GameLoop();
        }

        private void OnVerifiedInput(CommandType commandType, string input)
        {
            Command command = CommandFactory.CreateCommand(commandType, input);
        }

        private void GameLoop()
        {
            while (isActive)
            {
                inputHandler.DemandInput(OnVerifiedInput);
            }
        }

    }


    class Program
    {
        static void Main(string[] args)
        {
            Game game = new Game();
            game.Start();
        }
    }
}